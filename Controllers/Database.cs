﻿using System.Collections.Generic;
using BlackFriday.Models;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace BlackFriday.Controllers
{
    public class Database : DbContext
    {

        public Database()
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql("User ID=postgres;Password=111;Host=10.100.67.111;Port=5432;Database=bf;Pooling=true;");

        public DbSet<Good> Goods { get; set; }
    }
}
