﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BlackFriday.Models;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Npgsql;
using System.Net.Http.Headers;
using PagedList;
using PagedList.Mvc;
using Microsoft.AspNetCore.Http;

namespace BlackFriday.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private static readonly HttpClient client = new HttpClient();

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult black_friday()
        {
            return View("Index");
        }

        [HttpGet("/black-friday")]
        public IActionResult BlackFriday(int? page, string search, string filter)
        {
            using (Database context = new Database())
            {
                int pageSize = 16;
                int pageNumber = (page ?? 1);
                var list = context.Goods.OrderBy(o => o.category).ToList();
                if (!string.IsNullOrEmpty(search)) list = list.Where(q => q.title.Contains(search)).ToList();
                if (!string.IsNullOrEmpty(filter)) list = list.Where(q => q.category.Equals(filter)).ToList();
                return View(list.ToPagedList(pageNumber, pageSize));
            }
        }

        public IActionResult Add(int id)
        {
            using (Database context = new Database())
            {
                var product = context.Goods.SingleOrDefault(q => q.id == id);
                if(product != null)
                {
                    var products = HttpContext.Session.GetString("cart");
                    List<Good> goods = new List<Good>();
                    if (products != null)
                    {
                        goods = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Good>>(products);
                    }
                    goods.Add(product);
                    HttpContext.Session.SetString("cart", JsonConvert.SerializeObject(goods));
                }

                return RedirectToAction("Cart");
            }
        }


        public IActionResult Delete(int id)
        {
            var products = HttpContext.Session.GetString("cart");
            List<Good> goods = new List<Good>();
            if (products != null)
            {
                goods = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Good>>(products);
                Good deleted = goods.SingleOrDefault(q => q.id == id);
                goods.Remove(deleted);
            }
            HttpContext.Session.SetString("cart", JsonConvert.SerializeObject(goods));
            return RedirectToAction("Cart");
        }

        public IActionResult Cart()
        {
            var products = HttpContext.Session.GetString("cart");
            List<Good> goods = new List<Good>();
            if (products != null)
            {
                goods = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Good>>(products);
            }
            return View(goods);
        }

        public async Task<IActionResult> Update()
        {
            client.DefaultRequestHeaders
                .Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await client.GetStringAsync("http://chelhack.deletestaging.com/goods");
            var data = JsonConvert.DeserializeObject(response);
            JObject json = JObject.Parse(data.ToString());

            List<Good> goods = JsonConvert.DeserializeObject<List<Good>>(json.GetValue("data").ToString());
            if(goods == null || goods.Count == 0) return Json("Error");
            int i = 0;
            using (Database context = new Database())
            {
                try
                {
                    foreach (var item in goods)
                    {
                        item.parameters.ForEach(q => q.id = ++i);
                        context.Goods.Update(item);

                    }
                    context.SaveChanges();
                }
                catch (NpgsqlException ex)
                {
                    Debug.WriteLine(ex.Message);
                    Debug.Write(i);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    Debug.Write(i);
                }
            }

            return Json("Ok");
        }


        public async Task<IActionResult> Upload()
        {
            client.DefaultRequestHeaders
                .Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await client.GetStringAsync("http://chelhack.deletestaging.com/goods");
            var data = JsonConvert.DeserializeObject(response);
            JObject json = JObject.Parse(data.ToString());
            List<Good> goods = JsonConvert.DeserializeObject<List<Good>>(json.GetValue("data").ToString());
            if (goods == null || goods.Count == 0) return Json("Error");
            int i = 0;
            using (Database context = new Database())
            {
                try
                {
                    foreach(var item in goods)
                    {
                        item.parameters.ForEach(q => q.id = ++i);
                        context.Goods.Add(item);
                        
                    }
                    context.SaveChanges();
                }
                catch(NpgsqlException ex)
                {
                    Debug.WriteLine(ex.Message);
                    Debug.Write(i);
                }
                catch(Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                    Debug.Write(i);
                }

            }
            return Json("Ok"); 
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
