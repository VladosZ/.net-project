﻿using Quartz;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using BlackFriday.Models;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace BlackFriday.Controllers
{
    public class Jobs : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            HttpClient client = new HttpClient();
            var response = await client.GetStringAsync("http://chelhack.deletestaging.com/goods");
            JObject Jobject = JObject.Parse(response);

            List<Good> goods = JsonConvert.DeserializeObject<List<Good>>(Jobject.GetValue("data").ToString());
            if (goods == null || goods.Count == 0) return;

            using (Database _context = new Database())
            {

                _context.Goods.UpdateRange(goods);
                await _context.SaveChangesAsync();
            }
        }
    }
}
