﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace BlackFriday.Migrations
{
    public partial class FullGoods : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "availability",
                table: "Goods",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "brand",
                table: "Goods",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "brandId",
                table: "Goods",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "category",
                table: "Goods",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "finalprice",
                table: "Goods",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "image",
                table: "Goods",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "price",
                table: "Goods",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "quantity",
                table: "Goods",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Parameters",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    title = table.Column<string>(nullable: true),
                    value = table.Column<int>(nullable: false),
                    Goodid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Parameters", x => x.id);
                    table.ForeignKey(
                        name: "FK_Parameters_Goods_Goodid",
                        column: x => x.Goodid,
                        principalTable: "Goods",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Parameters_Goodid",
                table: "Parameters",
                column: "Goodid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Parameters");

            migrationBuilder.DropColumn(
                name: "availability",
                table: "Goods");

            migrationBuilder.DropColumn(
                name: "brand",
                table: "Goods");

            migrationBuilder.DropColumn(
                name: "brandId",
                table: "Goods");

            migrationBuilder.DropColumn(
                name: "category",
                table: "Goods");

            migrationBuilder.DropColumn(
                name: "finalprice",
                table: "Goods");

            migrationBuilder.DropColumn(
                name: "image",
                table: "Goods");

            migrationBuilder.DropColumn(
                name: "price",
                table: "Goods");

            migrationBuilder.DropColumn(
                name: "quantity",
                table: "Goods");
        }
    }
}
