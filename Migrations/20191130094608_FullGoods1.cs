﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BlackFriday.Migrations
{
    public partial class FullGoods1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "availability",
                table: "Goods",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "availability",
                table: "Goods",
                type: "integer",
                nullable: false,
                oldClrType: typeof(bool));
        }
    }
}
