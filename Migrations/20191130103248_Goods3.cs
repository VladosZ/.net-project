﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BlackFriday.Migrations
{
    public partial class Goods3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "image",
                table: "Goods");

            migrationBuilder.AddColumn<string>(
                name: "imageUrl",
                table: "Goods",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "imageUrl",
                table: "Goods");

            migrationBuilder.AddColumn<string>(
                name: "image",
                table: "Goods",
                type: "text",
                nullable: true);
        }
    }
}
