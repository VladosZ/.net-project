﻿// <auto-generated />
using System;
using BlackFriday.Controllers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace BlackFriday.Migrations
{
    [DbContext(typeof(Database))]
    partial class DatabaseModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn)
                .HasAnnotation("ProductVersion", "3.0.1")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("BlackFriday.Models.Good", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<bool>("availability")
                        .HasColumnType("boolean");

                    b.Property<string>("brand")
                        .HasColumnType("text");

                    b.Property<int>("brandId")
                        .HasColumnType("integer");

                    b.Property<string>("category")
                        .HasColumnType("text");

                    b.Property<int>("finalprice")
                        .HasColumnType("integer");

                    b.Property<string>("imageUrl")
                        .HasColumnType("text");

                    b.Property<int>("price")
                        .HasColumnType("integer");

                    b.Property<int>("quantity")
                        .HasColumnType("integer");

                    b.Property<string>("title")
                        .HasColumnType("text");

                    b.HasKey("id");

                    b.ToTable("Goods");
                });

            modelBuilder.Entity("BlackFriday.Models.Parameters", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

                    b.Property<int?>("Goodid")
                        .HasColumnType("integer");

                    b.Property<string>("title")
                        .HasColumnType("text");

                    b.Property<string>("value")
                        .HasColumnType("text");

                    b.HasKey("id");

                    b.HasIndex("Goodid");

                    b.ToTable("Parameters");
                });

            modelBuilder.Entity("BlackFriday.Models.Parameters", b =>
                {
                    b.HasOne("BlackFriday.Models.Good", null)
                        .WithMany("parameters")
                        .HasForeignKey("Goodid");
                });
#pragma warning restore 612, 618
        }
    }
}
