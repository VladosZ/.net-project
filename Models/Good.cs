﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace BlackFriday.Models
{
    public class Good
    {
        public int id { get; set; }

        [DisplayName("Наименование")]
        public string title { get; set; }
        public bool availability { get; set; }

        [DisplayName("Старая цена")]
        public int price { get; set; }

        [DisplayName("Крутая цена")]
        public int finalprice { get; set; }

        [DisplayName("Категория")]
        public string category { get; set; }

        [DisplayName("Производитель")]
        public string brand { get; set; }

        public int brandId { get; set; }

        [DisplayName("Доступное количество")]
        public int quantity { get; set; }

        [DisplayName("Картиночка")]
        public string imageUrl { get; set; }

        public List<Parameters> parameters { get; set; }
}


    public class Parameters
    {
        public int id { get; set; }

        [DisplayName("Характеристика")]
        public string title { get; set; }
        public string value { get; set; }
    }

}
